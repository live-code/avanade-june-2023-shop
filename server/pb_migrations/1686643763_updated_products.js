migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("upm17fxtwq25ual")

  collection.listRule = ""
  collection.viewRule = ""

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("upm17fxtwq25ual")

  collection.listRule = null
  collection.viewRule = null

  return dao.saveCollection(collection)
})
