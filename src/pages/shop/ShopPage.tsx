import { useEffect, useState } from 'react';
import { Product } from '../../model/product.ts';
import { pb } from '../../pocketbase.ts';
import { useCart, useCartPanel } from '../../services/cart';
import { getPBImgPath } from '../../shared/db/img.utils.ts';

export function ShopPage() {
  const [products, setProducts] = useState<Product[]>([]);
  const openPanel = useCartPanel(state => state.openPanel);
  const addToCart = useCart(state => state.addToCart)

  useEffect(() => {
    pb.collection('products').getList<Product>()
      .then(res => {
        setProducts(res.items)
      })
  }, [])

  return (
    <div>
      <h1 className="title">SHOP</h1>

      <div
        className="flex justify-around"
        data-testid="productList"
      >
      {
        products.map(p => {
          return <div key={p.id}>
            <img src={getPBImgPath('products', p)} className="w-24 rounded-xl"/>
            {p.name} - € {p.cost}

            <br/>
            <button
              onClick={() => {
                openPanel();
                addToCart(p);
              }}
              className="btn accent">add</button>
          </div>
        })
      }
      </div>
    </div>
  )
}

