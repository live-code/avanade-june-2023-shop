import React, { PropsWithChildren } from 'react';
import { Navigate } from 'react-router-dom';
import { useAuth } from '../../services/auth/useAuth.ts';

interface PrivateRouteProps {
  else?: React.ReactNode
}

export function PrivateRoute(props: PropsWithChildren<PrivateRouteProps>) {
  const isLogged = useAuth(state => state.isLogged)

  return <>
    {isLogged ? props.children : <Navigate to="/login" />}
  </>
}
