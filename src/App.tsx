import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { CartPage } from './pages/cart/CartPage.tsx';
import { Checkout } from './pages/cart/Checkout.tsx';
import { CMSPage } from './pages/cms/CMSPage.tsx';
import { LoginPage } from './pages/login/LoginPage.tsx';
import { ShopPage } from './pages/shop/ShopPage.tsx';
import { NavBar } from './shared';
import { PrivateRoute } from './shared/auth/PrivateRoute.tsx';
// import { NavBar } from './shared/components/core/NavBar.tsx';

function App() {
  return (
    <BrowserRouter>
      <NavBar />

      <div className="page">
        <Routes>
          <Route path="login" element={<LoginPage />} />
          <Route path="shop" element={<ShopPage />} />
          <Route path="cms" element={<PrivateRoute><CMSPage /></PrivateRoute>} />
          <Route path="cart" element={<CartPage />} />
          <Route path="checkout" element={<Checkout />} />
          <Route path="*" element={
            <Navigate to="shop" />
          } />
        </Routes>
      </div>

    </BrowserRouter>
  )
}

export default App
