import { create } from 'zustand'

export interface CartPanelState {
  isOpen: boolean,
  counter: number,
  inc: () => void,
  toggle: () => void;
  openPanel: () => void;
  closePanel: () => void;
}

export const useCartPanel = create<CartPanelState>((set, get) => ({
  isOpen: false,
  counter: 0,
  inc: () => set(s => ({ counter: s.counter + 1 })),
  toggle: () => set(s => ({ isOpen: !s.isOpen })),
  toggle2: () => set({ isOpen: !get().isOpen }),
  openPanel: () => set({ isOpen: true }),
  closePanel: () => set({ isOpen: false }),
}))

