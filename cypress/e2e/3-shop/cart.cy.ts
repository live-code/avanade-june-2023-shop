import { ListResult } from 'pocketbase';
import { Product } from '../../../src/model/product';

describe('shop tests', function () {
  beforeEach(() => {
    cy.viewport('iphone-7', 'landscape')
    cy.visit('http://localhost:5173/shop')
  })

  it('should contains SHOP title', function () {
    cy.get('.page').contains('SHOP')
    cy.get('.page').contains('shop', { matchCase: false })
    cy.screenshot('mySHop', { overwrite: true})
  });

  it('should display the first element name', function () {
    cy
      .get('[data-testid="productList"]')
      .children()
      .first()
      .should('contain', 'nutella')
  });

  it.skip('should add product cart', function () {
    cy
      .get('[data-testid="productList"]')
      .children()
      .first()
      .contains('add')
      .click()

    cy.contains('Total')
      .siblings()
      .contains('5')
  });

  it('should add product cart', function () {

    const mock: ListResult<Partial<Product>> = {
      items: [
        { id: '1', name: 'Pippo', cost: 10 },
        { id: '2', name: 'Pluto', cost: 100 },
      ]
    }
    cy.intercept(
      'http://127.0.0.1:8090/api/collections/products/records?page=1&perPage=30',
      { method: 'GET' },
      {
        statusCode: 200,
        body: mock
      }
    )
    cy
      .get('[data-testid="productList"]')
      .children()
      .first()
      .contains('add')
      .as('btn')

    cy.get('@btn').click()
    cy.get('@btn').click()
    cy.get('@btn').click()

    cy.contains('Total')
      .siblings()
      .should('have.text', mock.items[0].cost * 3)
  });

  it('should redirect to go to cart when button is clicked', function () {

    const mock: ListResult<Partial<Product>> = {
      items: [
        { id: '1', name: 'Pippo', cost: 10 },
        { id: '2', name: 'Pluto', cost: 100 },
      ]
    }
    cy.intercept(
      'http://127.0.0.1:8090/api/collections/products/records?page=1&perPage=30',
      { method: 'GET' },
      {
        statusCode: 200,
        body: mock
      }
    )
    cy
      .get('[data-testid="productList"]')
      .children()
      .first()
      .contains('add')
      .as('btn')

    cy.get('@btn').click()

    cy.contains('Total')
      .siblings()
      .should('have.text', mock.items[0].cost)

    cy.contains('Go to Cart').click()

    cy.url()
      .should('include', '/cart')
  });
});
